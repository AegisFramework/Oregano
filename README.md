# Aegis Framework: Oregano

Oregano is a simple template, this flavor features just the front-end technologies of Aegis instead of adding a Backend as the other flavors. It will allow you allow you to create your website quickly, featuring the cutting edge technologies.

It also features many properties to make your website mobile, social friendly and even a progressive web app.

Visit the [WebPage](https://www.aegisframework.com/)

## License
Aegis is an Open Source project released under the [MIT license](https://github.com/HyuchiaDiego/AegisTemplate/blob/master/LICENSE).